#!/usr/bin/env python

"""Export Google Search Appliance configuration as an XML file.

Requires config.ini and gsa_admin.py in the same folder as this file.
"""

import os
import smtplib
import ConfigParser
import gsa_admin  # http://code.google.com/p/gsa-admin-toolkit/source/browse/trunk/gsa_admin.py
from datetime import date
from socket import gethostname
from urllib2 import URLError

__author__ = "Dylan Barlett"
__copyright__ = "Arlington County Government"
__license__ = "GPLv3"
__version__ = "1.0.1"
__maintainer__ = "Dylan Barlett"
__email__ = "dbarlett@arlingtonva.us"
__status__ = "Production"

CONFIG_FILE = "config.ini"
MSG_TEMPLATE = (
    "From: {from_name} <{from_email}>\r\n"
    "To: {to_emails}\r\n"
    "Subject: {subject}\r\n\r\n"
    "{body}\r\n"
)

if __name__ == '__main__':
    file_suffix = "_{}.xml".format(date.today().strftime("%A"))

    try:
        parser = ConfigParser.SafeConfigParser()
        parser.read(CONFIG_FILE)

        output_name = parser.get("export", "path") + parser.get("gsa", "name") + file_suffix
        smtp_host = parser.get("email", "server")
        smtp_port = parser.get("email", "port")
        smtp_from = parser.get("email", "from_email")
        smtp_to = parser.get("email", "to_emails")

        gsa = gsa_admin.gsaWebInterface(
            parser.get("gsa", "hostname"),
            parser.get("gsa", "username"),
            parser.get("gsa", "password"),
            parser.get("gsa", "admin_port"),
        )
        gsac = gsa.exportConfig(parser.get("gsa", "sign_password"))

        # writeFile won't overwrite an existing export, so remove if present
        if os.path.exists(output_name):
            os.remove(output_name)
        gsac.writeFile(output_name)

        msg_subject = "GSA config exported"
        msg_body = "Exported configuration from {gsa_name} to {file_name} ({file_size:,} bytes).".format(
            gsa_name=parser.get("gsa", "name"),
            file_name=output_name,
            file_size=os.stat(output_name).st_size
        )
        error = None
    except ConfigParser.Error:
        error = "Error reading settings file ({filename})".format(filename=CONFIG_FILE)
    except URLError as e:
        error = "Error exporting config from {name}: {details}".format(
            name=parser.get("gsa", "name"),
            details=e,
        )

    print "Export complete"

    try:
        if error:
            msg_subject = "Error exporting GSA config"
            msg_body = error

        msg_formatted = MSG_TEMPLATE.format(
            from_name=gethostname(),
            from_email=smtp_from,
            to_emails=smtp_to,
            subject=msg_subject,
            body=msg_body,
        )
        print msg_formatted
        mailserver = smtplib.SMTP("{host}:{port}".format(
            host=smtp_host,
            port=smtp_port,
        ))

        mailserver.sendmail(
            from_addr=smtp_from,
            to_addrs=smtp_to.split(","),
            msg=msg_formatted,
        )
        mailserver.quit()
        print "Email sent"
    except smtplib.SMTPException as e:
        print e
