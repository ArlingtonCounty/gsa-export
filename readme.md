##GSA Export
Export a Google Search Appliance configuration to an XML file. Tested with Python 2.7.3 (32-bit) on Windows 7 (32-bit) and Windows Server 2008 R2 (64-bit) and GSA 7.0.14. No dependencies except the Python standard library and [gsa_admin.py](http://code.google.com/p/gsa-admin-toolkit/source/browse/trunk/gsa_admin.py) (included) from the [GSA Admin Toolkit](http://code.google.com/p/gsa-admin-toolkit/).

####Setup
1. Clone this repo
1. Rename `config.ini.example` to `config.ini` and fill in appropriate vaules

####Invocation
```sh
python export.py
```
####Sample email after successful export
```
From: SERVER-HOSTNAME [mailto:no-reply@example.com] 
Sent: Tuesday, February 04, 2014 8:23 AM
To: jdoe@example.com
Subject: GSA config exported

Exported configuration from GSA-NAME to C:\GSA Backups\GSA-NAME_Tuesday.xml (2,103,369 bytes).
```
####License
export.py is licensed under the GNU General Public License, Version 3 (see LICENSE). gsa_admin.py is licensed under the Apache License, Version 2.0.

####Copyright
2014 [Arlington County Government](http://www.arlingtonva.us)